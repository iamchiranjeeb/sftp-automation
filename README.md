## Basic Goproject to Automate SFTP Tasks

#### -> Configure a yml/json file (get the file from samplefile directory).

#### -> execute main.go with full path to config file,eg:- go run main.go --path full path to json/yml file.

#### -> if you re using password the set password:true if using rsa pvt key the password:false.

#### -> in auth enter you password or full path to rsa-pvt key.

#### -> help full for uploading file or directory to remote machine with minor change in json/yml file.
