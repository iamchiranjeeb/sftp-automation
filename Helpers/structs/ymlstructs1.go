package structs

type YamlStructure struct {
	localPath string `yaml:"localPath"`
	remotePath string `yaml:"remotePath"`
	user string `yaml:"user"`
	ip string `yaml:"ip"`
	port int `yaml:"port"`
	files []string `yaml:"files"`
	operation string `yaml:"operation"`
	ifCoommand bool `yaml:"execCommand"`
	command string `yaml:"command"`
}

type ParentYaml struct {
	Data []YamlStructure `yaml:"Data"`
}